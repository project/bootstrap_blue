A Blue sub-theme for Bootstrap. Inspired by Fervens.

This theme, although stable, is in its infancy.
Please report bugs and expect frequent feature changes and updates.

Installation
------------
1.  Unpack the downloaded file, place the entire theme folder in your Drupal
    installation under any one of the following directory:

      sites/all/themes         - Making it available to the default Drupal site
                                 and to all Drupal sites in a multi-site
                                 configuration.
      sites/default/themes     - Making it available to only the default Drupal
                                 site.
      sites/example.com/themes - Making it available to only the example.com
                                 site if there is a
                                 sites/example.com/settings.php configuration
                                 file.

2.  Log in as administrator on your Drupal site and go to Appearance settings
    (admin/appearance) and set it to the default theme.

Optional
--------
1.  To enable the theme features head to admin/appearance/settings/bootstrapblue
